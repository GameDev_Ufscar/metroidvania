extends Node

onready var current_scene = null
onready var player = get_node("../Player")

func _ready():
	current_scene = get_node("CurrentLevel")
	pass
		

func change_scene(target_level_name, target_warp_id, player_offset):
	var target_level = load(target_level_name).instance()
	call_deferred("_deferred_change_scene", target_level, target_warp_id, player_offset)
	
func _deferred_change_scene(target_level, target_warp_id, player_offset):
	current_scene.get_child(0).free()
	current_scene.add_child(target_level)
	
	for bullet in get_tree().get_nodes_in_group("player_bullets"):
		bullet.queue_free()
	
	for bullet in get_tree().get_nodes_in_group("enemy_bullets"):
		bullet.queue_free()
	
	for warp in get_tree().get_nodes_in_group("warps"):
		if warp.warp_id == target_warp_id:
			if warp.disable_after_warp:
				warp.disableWarp()
			player.position = warp.get_node("Position2D").get_global_position() + player_offset
			break
	
	
