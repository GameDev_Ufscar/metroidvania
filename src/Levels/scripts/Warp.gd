extends Area2D

onready var level_controller = get_node("/root/Game/LevelController")

export (String, FILE, "*.tscn, *.scn") var target_level_name
export (int) var target_warp_id
export (int) var warp_id
export (bool) var disable_after_warp = true
export (bool) var is_single_lane_warp = false
var enabled = true

func _ready():
	add_to_group("warps")
	
	if is_single_lane_warp:
		$CollisionShape2D.set_disabled(true)


func _on_Warp_body_entered(body):
	if body.name == "Player" and enabled:
		var player_offset = -(self.get_global_position() - body.get_global_position())
		if(self.scale.x > self.scale.y):
			player_offset.y = 0
		else:
			player_offset.x = 0
		level_controller.change_scene(target_level_name, target_warp_id, player_offset)


func disableWarp():
	enabled = false


func _on_Warp_body_exited(body):
	enabled = true