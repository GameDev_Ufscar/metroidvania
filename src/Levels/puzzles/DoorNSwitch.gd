extends StaticBody2D

onready var switch = get_parent().get_node("Switch")

func _on_Switch_area_entered(area):
	if area.is_in_group("whip_hitbox"):
		unlock()


func _on_Switch_body_entered(body):
	if body.is_in_group("player_bullets"):
		unlock()


func unlock():
	set_collision_layer_bit(0, 0)
	self.visible = false
	switch.modulate = Color(1, 1, 1, 1)
