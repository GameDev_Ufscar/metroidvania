extends Node2D

onready var Tilemap = $TileMap
onready var Global = $"/root/Global"

onready var gui = Global.gui

onready var size = get_size()
onready var limits = get_limits()
onready var enemy_spawners = $EnemySpawners

export (String) var AREA_NAME = "area"

var area_active setget set_area_active, is_area_active
var area_visit setget set_area_visit, is_area_visit
#var area_known setget set_area_known, is_area_known

func set_area_active(value: bool):
	area_active = value


func is_area_active():
	return area_active
	
	
func set_area_visit(value: bool):
	area_active = value


func is_area_visit():
	return area_active
	
	
func _ready():
	visible = false
	create_area2d()

func _activate_area(body):
	if body.name == "Player":
		self.set_area_active(true)

		gui.msg.set_text("Current Area: " + AREA_NAME)
		
		if enemy_spawners:
			for i in enemy_spawners.get_children():
				i.call_deferred("respawn")
		
		visible = true
		body.camera.limit_left = limits[0]
		body.camera.limit_top = limits[1]
		body.camera.limit_right = limits[2]
		body.camera.limit_bottom = limits[3]
	
	pass

func _deactivate_area(body):
	if body.name == "Player":
		self.set_area_active(false)
		
		if enemy_spawners:
			for i in enemy_spawners.get_children():
				i.call_deferred("despawn")
		
	if body.is_in_group("player_bullets"):
		body.call_deferred("stop")
	
	
func create_area2d():
	var shape = RectangleShape2D.new()
	shape.set_extents(Vector2(size.x/2 - 6, size.y/2 - 14))
	
	var collision = CollisionShape2D.new()
	collision.set_shape(shape)
	
	var area2d = Area2D.new()
	area2d.set_position(size/2)
	area2d.add_child(collision)
	area2d.connect("body_entered", self, "_activate_area")
	area2d.connect("body_exited", self, "_deactivate_area")
	
	add_child(area2d)

func get_size():
	var map_limits = Tilemap.get_used_rect()
	var map_cellsize = Tilemap.cell_size
	var left = map_limits.position.x * map_cellsize.x
	var right = map_limits.end.x * map_cellsize.x
	var top = map_limits.position.y * map_cellsize.y
	var bottom = map_limits.end.y * map_cellsize.y
	return Vector2(right - left, bottom - top)

func get_limits():
	var map_limits = Tilemap.get_used_rect()
	var map_cellsize = Tilemap.cell_size
	var left = map_limits.position.x * map_cellsize.x + global_position.x
	var right = map_limits.end.x * map_cellsize.x + global_position.x
	var top = map_limits.position.y * map_cellsize.y + global_position.y
	var bottom = map_limits.end.y * map_cellsize.y + global_position.y
	return [left, top, right, bottom]