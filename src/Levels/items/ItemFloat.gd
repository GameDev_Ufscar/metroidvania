extends Area2D

var float_direction = 1
var float_amount

func init_float():
	float_amount = 8 #$Sprite.texture.get_size().y
	position.y -= float_amount
	float_y(float_direction*float_amount)
	pass
	
func _process(delta):
	pass

func float_y(amount):
	$Tween.interpolate_property(self, "position", self.position, self.position+Vector2(0, amount), 1,
    Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	$Tween.start()
	
func _on_Tween_tween_completed(object, key):
	float_direction *= -1
	float_y(float_direction*float_amount)
	pass # Replace with function body.
