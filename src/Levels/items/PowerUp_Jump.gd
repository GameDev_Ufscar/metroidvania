extends "ItemFloat.gd"

func _ready():
	.init_float()
	pass


func _on_DoubleJump_body_entered(body):
	if body.name == "Player":
		body.multi_jump += 1
		$CollisionShape2D.set_disabled(true)
		$Sprite.visible = false
		$Timer.start()


func _on_Timer_timeout():
	$CollisionShape2D.set_disabled(false)
	$Sprite.visible = true