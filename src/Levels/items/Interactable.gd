extends Area2D

onready var player = Global.player

var wait_input = false

func _ready():
	self.connect("body_entered", self, "_on_body_entered")
	self.connect("body_exited", self, "_on_body_exited")
	
	
func _physics_process(delta):
	if wait_input:
		if player.just_lookup:
			interact()
	

func _on_body_entered(body):
	if body.name == "Player":
		wait_input = true
		
		
func _on_body_exited(body):
	if body.name == "Player":
		wait_input = false
		
		
func interact():
	pass

