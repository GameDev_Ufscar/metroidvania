extends "ItemFloat.gd"

onready var gui = get_tree().get_nodes_in_group("gui")[0]

func _ready():
	.init_float()
	pass
	
func _on_DoubleJump_body_entered(body):
	gui.msg_walljump()
	if body.name == "Player":
		body.WALLJUMP = true
		queue_free()