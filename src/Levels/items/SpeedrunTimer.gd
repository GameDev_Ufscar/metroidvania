extends Node

var start_timer = false

var time_start = 0
var time_now = 0
var str_elapsed = ""

func _ready():
    time_start = OS.get_unix_time()
    set_process(true)

func _process(delta):
	if start_timer:
		time_now = OS.get_unix_time()
		var elapsed = time_now - time_start
		var minutes = elapsed / 60
		var seconds = elapsed % 60
		str_elapsed = "Time %02d : %02d " % [minutes, seconds]
	else:
		time_start = OS.get_unix_time()
		time_now = 0