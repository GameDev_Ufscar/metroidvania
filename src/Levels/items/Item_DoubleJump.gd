extends "ItemFloat.gd"

onready var gui = get_tree().get_nodes_in_group("gui")

func _ready():
	.init_float()
	pass
	
func _on_DoubleJump_body_entered(body):
	if body.name == "Player":
		#gui.msg_doublejump()
		body.MAX_MULTIJUMP += 1
		queue_free()