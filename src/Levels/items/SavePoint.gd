extends "Interactable.gd"

func interact():
	get_tree().call_group("enemy_spawners", "set_defeated", false)
	player.stats.heal()
	player.last_savepoint = self
	
func respawn():
	get_tree().call_group("enemy_spawners", "set_defeated", false)
	player.global_position = self.global_position
	player.stats.heal()
