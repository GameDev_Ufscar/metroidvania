extends "ItemFloat.gd"


export (Color) var power_color


func _ready():
	.init_float()
	pass


func _on_DoubleJump_body_entered(body):
	if body.name == "Player":
		body.dash_counter_cooldown = body.DASH_COUNTER_COOLDOWN_MAX
		$CollisionShape2D.set_disabled(true)
		$Sprite.visible = false
		$Timer.start()
		body.change_cloak_color(power_color)


func _on_Timer_timeout():
	$CollisionShape2D.set_disabled(false)
	$Sprite.visible = true
