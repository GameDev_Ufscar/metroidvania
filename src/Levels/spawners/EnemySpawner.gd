extends Position2D

enum TYPE {COMMON, BOSS}


export (Resource) var enemy
export (TYPE) var enemy_type


var defeated = false setget set_defeated, is_defeated


func is_defeated():
	return defeated
	
	
func set_defeated(d):
	defeated = d
	
	if d:
		$MinimapPin.z_index = 0
	else:
		$MinimapPin.z_index = 5


func _ready():
	add_to_group("enemy_spawners")

	if get_child(1):
		despawn()


func respawn():
	if !defeated and enemy_type == TYPE.COMMON:
		var new_enemy = enemy.instance()
		new_enemy.connect("enemy_defeated", self, "_on_enemy_defeated")
		
		add_child(new_enemy)
		$MinimapPin.z_index = 0


func despawn():
	if get_child(1):
		get_child(1).call_deferred("queue_free")
	
	if !defeated:
		$MinimapPin.z_index = 5


func _on_enemy_defeated():
	set_defeated(true)
