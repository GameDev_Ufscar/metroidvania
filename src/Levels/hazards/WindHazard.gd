extends Area2D

export (int) var wind_force = 50

func _ready():
	if wind_force > 0:
		$Particles2D.emitting = true
	else:
		$Particles2D2.emitting = true

func _on_Wind_body_entered(body):
	if body.is_in_group("player"):
		body.WIND.x = wind_force


func _on_Wind_body_exited(body):
	if body.is_in_group("player"):
		body.WIND.x = 0
