extends Area2D


func _on_Spike_body_entered(body):
	if body.is_in_group("player"):
		body.player_take_damage_and_knockback(1)
		
	if body.is_in_group("enemies"):
		body.enemy_take_damage_and_knockback(99)
