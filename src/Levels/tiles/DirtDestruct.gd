extends StaticBody2D

func _ready():
	add_to_group("destructables")
	
	
func _on_Hitbox_area_entered(area):
	if area.is_in_group("whip_hitbox"):
		call_deferred("destroy")
		
		
func destroy():
	set_collision_layer_bit(0, 0)
	$CollisionShape2D.disabled = true
	$Sprite.visible = false
	$Particles2D.emitting = true
	yield(get_tree().create_timer(1.5), "timeout")
	queue_free()