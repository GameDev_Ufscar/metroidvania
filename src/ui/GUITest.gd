extends CanvasLayer

onready var start_time = OS.get_ticks_msec()
onready var msg = $GUI/Messages/DoubleJump
onready var hp = $GUI/Messages/HP


func _ready():
	add_to_group("gui")
	
	
func change_hp(value):
	hp.set_text("HP: " + str(value) )
