extends CheckBox

func is_radio():
	pass

func _ready():
	self.connect("toggled", self, "_on_RadioButton_toggled", [Input.is_action_just_pressed("ui_mouse_left")])

func _on_RadioButton_toggled(button_pressed):
	if button_pressed:
		for child in get_parent().get_children():
			if child.has_method("is_radio"):
				child.pressed = false
				pressed = true
		get_parent().player.change_suit(self.text)