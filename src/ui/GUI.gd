extends CanvasLayer

onready var anim = $GUI/anim
onready var start_time = OS.get_ticks_msec()
onready var player = get_node("/root/Game/Player")


func _ready():
	add_to_group("gui")
	
	$GUI/Messages/HP.text = "HP: 10"


func fade_in():
	anim.play("fade_in")


func fade_in_final_perfect():
	SpeedrunTimer.start_timer = false
	$GUI/Messages/time.text = SpeedrunTimer.str_elapsed
	anim.play("fade_in_final_perfect")


func fade_in_final():
	SpeedrunTimer.start_timer = false
	$GUI/Messages/time.text = SpeedrunTimer.str_elapsed
	anim.play("fade_in_final")


func fade_out():
	anim.play("fade_out")
	
	
func msg_basic():
	SpeedrunTimer.start_timer = true
	anim.play("msg_basic")
	

func msg_dash():
	anim.play("msg_dash")
	
	
func msg_walljump():
	anim.play("msg_walljump")
	
	
func msg_doublejump():
	anim.play("msg_doublejump")
	
	
func set_hp_value(v):
	$GUI/Messages/HP.text = "HP: " + str(v)