extends Node2D

export (String, FILE, "*.tscn, *.scn") var Wave
export var damage = 1

var maxWave = 0
var numRep = 0
var nextWave = 1
var nextWavePosition = Vector2.ZERO

onready var nextWavePosi = $NextWavePosition

func start(_transform, _damage, _numRep, _maxWave):
	if _maxWave != 0:
		if _numRep > _maxWave:
			queue_free()
	
	position = _transform
	add_to_group("enemy_bullets")
	damage = _damage
	numRep = _numRep
	maxWave = _maxWave


func _on_NextWave_body_entered(body):
	if body.name == "TileMap":
		nextWave = 0

func _on_DamageArea_body_entered(body):
	if body.name == "Player":
		body.player_take_damage_and_knockback(damage, body.orientation*-1, Vector2(128, -128), 14 )


func _on_AnimationPlayer_animation_finished(anim_name):
	if (anim_name == "Wave"):
		queue_free()
		


func newWave():
	if(nextWave == 1):
		var proj = load(Wave).instance()
		proj.start(nextWavePosi.get_global_position()-get_parent().get_position(), damage, numRep+1, maxWave)
		get_node("../").add_child(proj)

