extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal startAttack
signal stopAttack
signal dealDamage

func _on_AttackHitbox_body_entered(body):
	if body.name == "Player":
		emit_signal("startAttack")


func _on_AttackHitbox_body_exited(body):
	if body.name == "Player":
		emit_signal("stopAttack")


func _on_Damage_body_entered(body):
	if body.name == "Player":
		emit_signal("dealDamage",body)

