extends KinematicBody2D

export (int) var WALK_SPEED = 64
export (int) var STARTER_DIRECTION = 1
export (int)var AIR_FINAL_SPEED = 550
export (int)var GRAV = 16


onready var sprite = $Sprite
onready var stats = $Stats
onready var iframes = $Timer
onready var player = $"/root/Global".player


var motion = Vector2(0,0)
var FALL_DIR = Vector2(0,-1)
var SPEED = 0
var KB_SPEED = 0
var snap = Vector2(0,31)
var orientation = 1
var state
var jumped = false


enum ENEMY {
	MOVE,
	AIR,
	STAGGER
}


func _ready():
	add_to_group("enemies")
	change_orientation(STARTER_DIRECTION)
	change_state(ENEMY.MOVE)
	
	
func _physics_process(delta):
	match state:
		ENEMY.MOVE:
			jumped = false
			SPEED = WALK_SPEED
			
			snap = Vector2(0,31)
			
			if is_on_wall():
				change_orientation(orientation*(-1))
			
			if !is_on_floor():
				change_state(ENEMY.AIR)
			
			if player.is_ground_jump:
				jumped = true
				motion.y = -400
				snap = Vector2(0,0)
				change_state(ENEMY.AIR)
		ENEMY.AIR:
			SPEED = WALK_SPEED
			
			jumped = true
			
			if is_on_wall():
				change_orientation(orientation*(-1))
			if is_on_floor():
				change_state(ENEMY.MOVE)
		ENEMY.STAGGER:
			SPEED = KB_SPEED
	
	if state != ENEMY.STAGGER:
		motion.x = SPEED * orientation
	else:
		motion.x = SPEED

	motion.y = approach(motion.y, AIR_FINAL_SPEED, GRAV)
	
	motion = move_and_slide_with_snap(motion, snap, FALL_DIR, true, 4, 0.79)
	
	$debug/hp.text = str(stats.health)
	$debug/speed.text = str(state)


func enemy_take_damage_and_knockback(
		damage: int = 0,
		source: KinematicBody2D = null,
		ori: int = 0,
		kb_speed: Vector2 = Vector2(0, 0),
		iframes_cooldown: float = 0):
	
	
	stats.take_damage(damage)
	
	if ori != 0:
		KB_SPEED = kb_speed.x * ori
	
	self.set_collision_mask_bit(3, false)

	if iframes_cooldown:
		iframes.start(iframes_cooldown)
		change_state(ENEMY.STAGGER)


func _on_Timer_timeout():
	self.set_collision_mask_bit(3, true)
	if jumped:
		change_state(ENEMY.AIR)
	else:
		change_state(ENEMY.MOVE)
	pass # Replace with function body.


func change_state(new_state):
	if(new_state != state):
		state = new_state


func change_orientation(ori):
	if(orientation != ori and ori != 0):
		orientation = ori
		sprite.scale.x *= -1
		
		
func approach(a, b, amount):
	if (a < b):
		a += amount
		if (a > b):
			return b
	else:
		a -= amount
		if(a < b):
			return b
	return a


func _on_EnemyHitbox_body_entered(body):
	if body.name == "Player":
		body.player_take_damage_and_knockback(stats.attack, orientation, Vector2(128, -128), 14 )
	pass


func _on_Stats_health_depleted():
	self.set_collision_mask_bit(3, false)
	queue_free()
