extends Node2D

export (String, FILE, "*.tscn, *.scn") var Wave
var damage = 1
var attack
var player
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Timer_timeout():
	if attack == 1:
		var proj = load(Wave).instance()
		proj.start(player.position, damage,get_node("Position2D").get_global_position())
		get_node("../").add_child(proj)
	pass # Replace with function body.


func _on_Area2D_body_entered(body):
	if(body.name == "Player"):
		player = body
		attack = 1
	pass # Replace with function body.
