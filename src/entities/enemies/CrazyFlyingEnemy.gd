extends KinematicBody2D

export (int) var FLY_SPEED = 64
export (int) var DAMAGE = 1
export (bool) var IS_BULLET_PROOF = false
export var steer_force = 50.0
export var finalSpeedMult = 1.4

onready var sprite = $Sprite
onready var stats = $Stats
onready var iframes = $Timer


var IDLE_SPEED = FLY_SPEED/2

var acceleration = Vector2.ZERO
var target = null
var motion = Vector2.ZERO
var randX
var randY
var accelerationIdle = 8
var state
var timeIdle = 0

enum ENEMY {
	IDLE,
	ATTACKING
}

func _ready():
	add_to_group("enemies")
	state = ENEMY.IDLE

func _physics_process(delta):
	if(motion.x >= 0):
		sprite.scale.x = -1
	else:
		sprite.scale.x = 1
		
	match state:
		ENEMY.IDLE:
			IdlleMove(delta)
			move_and_slide(motion)
			
		ENEMY.ATTACKING:
			acceleration += seek()
			motion += acceleration * delta
			motion = motion.clamped(FLY_SPEED)
			position += motion * delta * finalSpeedMult
			move_and_collide(motion)

func seek():
    var steer = Vector2.ZERO
    if target:
        var desired = (target.position - position).normalized() * FLY_SPEED
        steer = (desired - motion).normalized() * steer_force
    return steer

func  IdlleMove(var delta):
	if(timeIdle > 0):
		if(!(randX == 0 and motion.x > -1 * accelerationIdle and motion.x < accelerationIdle)):
			if(motion.x < randX * IDLE_SPEED):
				motion.x = motion.x + accelerationIdle
			else:
				if(motion.x > randX * IDLE_SPEED):
					motion.x = motion.x - accelerationIdle
				
		if(motion.y < randY * IDLE_SPEED):
			motion.y = motion.y + accelerationIdle
		else:
			if(motion.y > randY * IDLE_SPEED):
				motion.y = motion.y - accelerationIdle
		
		timeIdle = timeIdle - delta
	else:
		randX = int(rand_range(-5,3))+1
		randY = int(rand_range(-5,3))+1
		timeIdle = 0.5


func _on_Aggrobox_body_entered(body):
	if body.name == "Player":
		target = body
		state = ENEMY.ATTACKING
	pass
	
	
func _on_Aggrobox_body_exited(body):
	if body.name == "Player":
		target = null
		state = ENEMY.IDLE
	pass
	

func _on_EnemyHitbox_body_entered(body):
	if body.name == "Player":
		body.player_take_damage_and_knockback(stats.attack)
	pass



func enemy_take_damage_and_knockback(
		damage: int = 0,
		source: KinematicBody2D = null,
		ori: int = 0,
		kb_speed: Vector2 = Vector2(0, 0),
		iframes_cooldown: float = 0) -> bool:
	
	if source:
		if IS_BULLET_PROOF and source.is_in_group("player_bullets"):
			return false
	
	stats.take_damage(damage)
		
	return true


func _on_Stats_health_depleted():
	queue_free()
	pass # Replace with function body.
