extends KinematicBody2D

export (int) var WALK_SPEED = 64
export (int) var STARTER_DIRECTION = 1
export (int) var AIR_FINAL_SPEED = 550
export (int) var GRAV = 16
export (bool) var IS_BULLET_PROOF = false

onready var sprite = $Sprite
onready var attackArea = $Attack
onready var floor_detection = $Sprite/RayCast2D
onready var stats = $Stats
onready var iframes = $Timer
onready var anim = $Anim

var fimAttackAnim = false
var motion = Vector2(0,0)
var FALL_DIR = Vector2(0,-1)
var SPEED = 0
var KB_SPEED = 0
var orientation = -1
var state

#bug onde o inimigo para de atacar e comeca a andar, mesmo com o player
#dentro da area, causado pelo player "colar" no inimigo, possivel problema

enum ENEMY {
	MOVE,
	AIR,
	STAGGER,
	ATTACKING
}


func _ready():
	add_to_group("enemies")
	change_orientation(STARTER_DIRECTION)
	state = ENEMY.AIR



func _physics_process(delta):	
	match state:
		ENEMY.MOVE:
			anim.play("Walking")
			SPEED = WALK_SPEED
			
			if is_on_wall():
				change_orientation(orientation*(-1))
			elif !floor_detection.is_colliding():
				if is_on_floor():
					change_orientation(orientation*(-1))
				else:
					change_state(ENEMY.AIR)
		ENEMY.AIR:
			SPEED = 0
			
			if is_on_floor():
				change_state(ENEMY.MOVE)
				
		ENEMY.STAGGER:
			SPEED = KB_SPEED
			anim.play("Stagger")
			
		ENEMY.ATTACKING:
			SPEED = 0
			anim.play("Attack")
	
	
	if state == ENEMY.MOVE:
		motion.x = SPEED * orientation
	else:
		motion.x = SPEED
		
	
	
	motion.y = approach(motion.y, AIR_FINAL_SPEED, GRAV)
	
	motion = move_and_slide_with_snap(motion, Vector2(0,31), FALL_DIR, true, 4, 0.79)



func enemy_take_damage_and_knockback(
		damage: int = 0,
		source: KinematicBody2D = null,
		ori: int = 0,
		kb_speed: Vector2 = Vector2(0, 0),
		iframes_cooldown: float = 0) -> bool:
	
	if source:
		if IS_BULLET_PROOF and source.is_in_group("player_bullets"):
			return false
	
	stats.take_damage(damage)
	
	
	if ori != 0:
		KB_SPEED = kb_speed.x * ori
	
	self.set_collision_mask_bit(3, false)

	if iframes_cooldown != 0:
		iframes.start(iframes_cooldown)
		change_state(ENEMY.STAGGER)
		
	return true


func _on_EnemyHitbox_body_entered(body):
	if body.name == "Player":
		body.player_take_damage_and_knockback(stats.attack, body.orientation*-1, Vector2(128, -128), 14 )


func approach(a, b, amount):
	if (a < b):
		a += amount
		if (a > b):
			return b
	else:
		a -= amount
		if(a < b):
			return b
	return a



func change_orientation(ori):
	if(orientation != ori and ori != 0):
		orientation = ori
		sprite.scale.x *= -1



func _on_Timer_timeout():
	self.set_collision_mask_bit(3, true)
	change_state(ENEMY.MOVE)
	iframes.stop()


func _on_Stats_health_depleted():
	emit_signal("enemy_defeated")
	self.set_collision_mask_bit(3, false)
	queue_free()



func change_state(new_state):
	if(new_state != state):
		state = new_state


func _on_Anim_animation_finished(anim_name):
	if(anim_name == "Attack" and fimAttackAnim):
		fimAttackAnim = false
		change_state(ENEMY.MOVE)


func _on_Attack_startAttack():
	change_state(ENEMY.ATTACKING)


func _on_Attack_stopAttack():
	fimAttackAnim = true


func _on_Attack_dealDamage(body):
	body.player_take_damage_and_knockback(stats.attack*2, orientation, Vector2(128, -128), 14 )
