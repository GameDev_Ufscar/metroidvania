#nodes:
#
#WaveAttackPosition - posicao de começo da wave, precisa ser 
#mudado para encostar no chao e colocado numa posição onde 
#ela fica exatamente em cima de um tile para que nao fique 
#estranho no fim da parede
#
#HandPosition - posicao onde a mao começa
#LimitHand - limite de onde a mao avança para o soco
#
#HoamingBuletPosition - nao esta sendo usado devido ao 
#jeito estranho que as balas se comportaram, basta 
#chamar a funcao que gera ela se quiser coloca-las de novo
#


#-----------------------------------------------------------------------------------------------------------------
#começo do codigo

extends KinematicBody2D

#arquivos exportados da wave e da bala que percegue
export (String, FILE, "*.tscn, *.scn") var Wave
export (String, FILE, "*.tscn, *.scn") var HomingBullet

#varivel que decide se o inimigo é aprova de balas
export (int) var IS_BULLET_PROOF = 0
#variavel que define o tamanho da wave, zero igual até o limite da fase
#nao esta sendo usada, pode apagar
export (int) var maxWave = 0
#dano da wave
export (int) var damageWave = 2
#dano da bala
export (int) var damageHomingBullet = 2
#dano da mao e do body do boss
export (int) var damageHand = 2
#variavel que determina o intervalo inicial entre um ataque e outro, 
#para o ataque wave
export (float) var timerWave = 3
#variavel que determina a velocidade inicial da mao junto com o 
#intervalo entre um ataque e outro para o soco
export (float) var timeHandAttack = 2

#variaveis para os nodes e o player
onready var tween = $Hand/Tween
onready var timer = $Timer
onready var handTimer = $DelayHand
onready var anim = $AnimationPlayer
onready var player = $"/root/Global".player
onready var stats = $Stats
onready var Bosshit = $CollisionShape2D
onready var lifeBar = $LifeBarBack/LifeBar


#variavel usada para passar a posicao final da mao
var limitPosition = Vector2.ZERO
#variavel usada para definir o numero de repeticoes da wave
var numRep = 0
#variavel usada para determinar qual ataque sera feito a seguir
var attackPattern
#variavel que determina se o proximo ataque pode ser lançado
var attackReady
#variavel que determina qual a situacao da mao
var AttackHandStatus
#variavel que determina o antigo lvl do player, possivelmente inutil
var oldlvl = 1
#variavel usada no codigo para determinar a duracao do 
#intervalo dos ataques variando com o tempo
var timerWaveIn = timerWave
#variavel que determina a velocidade da mao, junto com o 
#intervalo entre ataques, variando com o tempo
var timeHandAttackIn = timeHandAttack
var porcentagemLifeBar
var maxHealth
var lifeBarScale
#inicio, ativa a variavel que incia o primeiro ataque e 
#decida qual sera.
#pode usar o timer para adicionar um delay antes do boss começar
func _ready():
	add_to_group("enemies")
	attackReady = 0
	attackPattern = int(rand_range(0,2))
	maxHealth = stats.max_health
	lifeBarScale = lifeBar.scale.x
	pass # Replace with function body.


func _process(delta):
#verifica se o ataque esta pronto para ser usado, qual sera
#e incia os mesmos
	if(attackReady == 1):
		if attackPattern == 0:
			#começa a animacao da wave, para alterar a wave em si
			#tem que mudar o anim da scene wave
			anim.play("WaveAttack")
			attackReady = 0
			timer.start(timerWaveIn);
			
		elif attackPattern == 1:
			attackReady = 0
			#muda o status da mao para atacando
			AttackHandStatus = 1
			#salva a posicao final da mao
			limitPosition.x = get_node("LimitHand").get_global_position().x
			limitPosition.y = get_node("HandPosition").get_global_position().y
			#incia o soco, pode adicionar uma multiplicacao ao 
			#limitPosition para aumentar ou diminuir a velocidade 
			#do golpe na ida da mao
			tween.interpolate_property(get_node("Hand"),"global_position",get_node("HandPosition").get_global_position(),limitPosition,timeHandAttackIn,Tween.TRANS_CUBIC,Tween.EASE_IN)                                                                   
			#timer que a mao chegou no ponto maximo e insere 
			#um pequeno delay ate o retorno da mao, pode mudar 
			#a mutiplicacao para aumentar ou diminuir esse 
			#delay, se for mudado a velocidade em cima, 
			#mudar aqui tbm
			handTimer.start(timeHandAttackIn + timeHandAttackIn * 0.4)
			tween.start()
	
	#verifica qual se a mao chegou ao fim do caminho e o tempo 
	#de delay ate a volta acabou e incia a volta da mao
	if AttackHandStatus == 2:
		#animacao da mao voltando, para acelerar essa parte 
		#especifica mude o multiplicador do timeHandAttackIn na 
		#funcao abaixo
		tween.interpolate_property(get_node("Hand"),"global_position",get_node("Hand").get_global_position(),get_node("HandPosition").get_global_position(),timeHandAttackIn*2,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)                                                                   
		tween.start()
		#tempo que leva para a mao voltar + tempo até o proximo 
		#ataque, se foi adicionado um multiplicador em cima mudar 
		#aqui tbm
		handTimer.start(timeHandAttackIn*2 + timeHandAttackIn * 0.3)
		#indica que a mao esta voltando ou que esta parada
		AttackHandStatus = 0
	pass

#funcao responsavel pela mudanca dos lvls, a multiplicacao 
#determina quao mais rapido o inimigo ira ficar, quanto 
#menor o valor final mais rapido esses valores influenciam 
#em todos os intervalos de seus perspectivos ataques
func changeLevel(var lvl):
	if oldlvl == 1 and lvl == 2:
		oldlvl = 2
		timerWaveIn = timerWave * 0.7
		timeHandAttackIn = timeHandAttack * 0.7
	elif oldlvl == 2 and lvl == 3:
#o lvl 3 esta colocando o bos como aprova de balas, se 
#preferir pode remover isso
		oldlvl = 3
		IS_BULLET_PROOF = 1
		timerWaveIn = timerWave * 0.45
		timeHandAttackIn = timeHandAttack * 0.45

#funcao que starta a wave
func AttackWave():
	var proj = load(Wave).instance()
	proj.start(get_node("WaveAttackPosition").get_global_position()-get_parent().get_position(), damageWave, numRep, maxWave)
	get_node("../").add_child(proj)

#funcao que dispara o tiro
func ShotHomingBullet():
	var proj = load(HomingBullet).instance()
	proj.start(get_node("HoamingBuletPosition").get_global_transform(), -320, damageHomingBullet)
	get_node("../").add_child(proj)


#funcao que determina que o ataque anterior acabou e que 
#o proximo pode comcar
func _on_Timer_timeout():
	attackReady = 1
	attackPattern = int(rand_range(0,2))
	timer.stop()


#funcao que determina a morte do boss
func _on_Node_health_depleted():
	queue_free()


#funcao que determina a mudanca do nivel do boss, e a barra de vida
func _on_Node_health_changed(old_value, new_value):
	print(new_value)
	if new_value < 15:
		changeLevel(3)
	elif new_value < 30:
		changeLevel(2)
		
	porcentagemLifeBar = lifeBarScale/maxHealth
	var diferenca = old_value - new_value
	lifeBar.scale.x = lifeBar.scale.x - diferenca*porcentagemLifeBar
	lifeBar.position.x = lifeBar.position.x - diferenca*porcentagemLifeBar


#funcao que finaliza as outras animacoes(no caso apenas a da wave) 
#e sete ele como iddle
func _on_AnimationPlayer_animation_finished(anim_name):
	anim.play("Iddle")


#funcao que determina se o player ou balas acertaram a mao
func _on_AreaHand_body_entered(body):
		if body.name == "Player":
			body.player_take_damage_and_knockback(damageHand)
		elif(body.name != "Boss"):
			body.queue_free()


#funcao responsavel pelos estados da mao e quando o ataque terminou
func _on_DelayHand_timeout():
	if AttackHandStatus == 1:
		AttackHandStatus = 2
	elif AttackHandStatus == 0:
		attackReady = 1
		attackPattern = int(rand_range(0,2))
	handTimer.stop()


#funcao que causa dano ao player quando invade a hitbox do boss
func _on_BossHitBox_body_entered(body):
	if body.name == "Player":
		body.player_take_damage_and_knockback(damageHand, body.orientation*-1, Vector2(128, -128), 14 )

#funcao que causa dano ao boss
func enemy_take_damage_and_knockback(
		damage: int = 0,
		source: KinematicBody2D = null,
		ori: int = 0,
		kb_speed: Vector2 = Vector2(0, 0),
		iframes_cooldown: float = 0) -> bool:
	
	if source:
		if IS_BULLET_PROOF and source.is_in_group("player_bullets"):
			return false
	
	stats.take_damage(damage)
	return true

func _on_PlayerDetect_body_entered(body):
	if(body.name == "Player"):
		attackReady = 1
	pass # Replace with function body.
