extends KinematicBody2D

var velocity = Vector2.ZERO
var acceleration = Vector2.ZERO
var speed
var damage

func start(_transform, _speed, _damage):
	add_to_group("enemy_bullets")
	global_transform = _transform
	speed = _speed
	velocity = transform.x * speed
	damage = _damage

func _physics_process(delta):
	velocity += acceleration * delta
	velocity = velocity.clamped(speed)
	rotation = velocity.angle()
	
	var collision = move_and_collide(velocity * delta)
	
	if collision:
		handle_collision(collision)


func handle_collision(collision):
	queue_free()
	
	
func _on_Hitbox_body_entered(body):
	if body.is_in_group("player"):
		body.player_take_damage_and_knockback(damage)
		queue_free()
