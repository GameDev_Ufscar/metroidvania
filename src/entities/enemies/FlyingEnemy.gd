extends KinematicBody2D

export (int) var FLY_SPEED = 32
export (int) var DAMAGE = 1
export (bool) var IS_BULLET_PROOF = false

onready var sprite = $Sprite
onready var stats = $Stats
onready var iframes = $Timer



var motion = Vector2(0,0)
var randX
var randY
var acceleration = 8
var timeIdle = 0



func _ready():
	add_to_group("enemies")

func _physics_process(delta):
	if(motion.x >= 0):
		sprite.scale.x = -1
	else:
		sprite.scale.x = 1
	
	IdlleMove(delta)
	move_and_slide(motion)


func  IdlleMove(var delta):
	if(timeIdle > 0):
		if(!(randX == 0 and motion.x > -1 * acceleration and motion.x < acceleration)):
			if(motion.x < randX * FLY_SPEED):
				motion.x = motion.x + acceleration
			else:
				if(motion.x > randX * FLY_SPEED):
					motion.x = motion.x - acceleration
					
		if(motion.y < randY * FLY_SPEED):
			motion.y = motion.y + acceleration
		else:
			if(motion.y > randY * FLY_SPEED):
				motion.y = motion.y - acceleration
				
		timeIdle = timeIdle - delta
	else:
		randX = int(rand_range(-5,3))+1
		randY = int(rand_range(-5,3))+1
		timeIdle = 0.5



func _on_EnemyHitbox_body_entered(body):
	if body.name == "Player":
		body.player_take_damage_and_knockback(stats.attack)
	pass



func enemy_take_damage_and_knockback(
		damage: int = 0,
		source: KinematicBody2D = null,
		ori: int = 0,
		kb_speed: Vector2 = Vector2(0, 0),
		iframes_cooldown: float = 0) -> bool:
	
	if source:
		if IS_BULLET_PROOF and source.is_in_group("player_bullets"):
			return false
	
	stats.take_damage(damage)
		
	return true


func _on_Stats_health_depleted():
	queue_free()
	pass # Replace with function body.
