extends KinematicBody2D

export (String, FILE, "*.tscn, *.scn") var bullet
export (int) var SHOOT_SPEED = 200
export (int) var DAMAGE = 1
export (bool) var IS_BULLET_PROOF = true


onready var sprite = $Sprite


func _ready():
	add_to_group("enemies")
	$AnimationPlayer.play("shooting")


func shoot():
	var proj = load(bullet).instance()

	proj.start(transform, SHOOT_SPEED, DAMAGE)

	get_node("../").add_child(proj)


func enemy_take_damage_and_knockback(
		damage: int = 0,
		source: KinematicBody2D = null,
		ori: int = 0,
		kb_speed: Vector2 = Vector2(0, 0),
		iframes_cooldown: float = 0) -> bool:
	return false


func _on_EnemyHitbox_body_entered(body):
	if body.name == "Player":
		body.player_take_damage_and_knockback(DAMAGE, body.orientation * -1, Vector2(128, -128), 14 )


