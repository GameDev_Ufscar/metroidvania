extends KinematicBody2D

var velocity = Vector2.ZERO
var acceleration = Vector2.ZERO
export var steer_force = 50.0
var speed
var damage
var target = null


func seek():
    var steer = Vector2.ZERO
    if target:
        var desired = (target.position - position).normalized() * speed
        steer = (desired - velocity).normalized() * steer_force
    return steer


func start(_transform, _speed, _damage):
	add_to_group("enemy_bullets")
	global_transform = _transform
	speed = _speed
	velocity = transform.x * speed
	damage = _damage

func _physics_process(delta):
	acceleration += seek()
	velocity += acceleration * delta
	velocity = velocity.clamped(speed)
	rotation = velocity.angle()
	position += velocity * delta
	
	var collision = move_and_collide(velocity * delta)
	
	if collision:
		handle_collision(collision)


func handle_collision(collision):
	queue_free()
	
	
func _on_Hitbox_body_entered(body):
	if body.is_in_group("player"):
		body.player_take_damage_and_knockback(damage)
		queue_free()


func _on_Area2D_body_entered(body):
	if body.is_in_group("player"):
		target = body


func _on_Timer_timeout():
	queue_free()


func _on_VisibilityNotifier_screen_exited():
	queue_free()
	pass # Replace with function body.
