extends KinematicBody2D


onready var Global = $"/root/Global"


var direction # Direction Vector
var orientation # When fixed movement
var init_speed


func init(dir, ori, speed: int = 0 ):
	direction = dir
	orientation = ori
	init_speed = speed
	$VisibilityNotifier2D.connect( "viewport_exited", self, "_on_VisibilityNotifier2D_viewport_exited" )
	
	
func _on_VisibilityNotifier2D_viewport_exited(viewport):
	if viewport.name == "GameViewport":
		queue_free()