extends Area2D


export (String) var TYPE = "body"


func _ready():
	var node_function = "_on_" + name + "_" + TYPE + "_entered"
	var node_function_exited = "_on_" + name + "_" + TYPE + "_exited"
	if TYPE == "body":
		connect("body_entered", get_parent(), node_function)
		connect("body_exited", get_parent(), node_function_exited)
	elif TYPE == "area":
		connect("area_entered", get_parent(), node_function)
		connect("area_exited", get_parent(), node_function_exited)
	elif TYPE == "both":
		connect("body_entered", get_parent(), node_function)
		connect("body_exited", get_parent(), node_function_exited)
		connect("area_entered", get_parent(), node_function)
		connect("area_exited", get_parent(), node_function_exited)

