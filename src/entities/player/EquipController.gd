extends Sprite


onready var player = get_parent()


var suits = {
	"Base": PoolColorArray([Color(0.25,0.70,0.76,1), Color(0.13,0.46,0.41,1), Color(0.13,0.25,0.46,1), 
			Color(0.47,0.54,0.55,1), Color(0.04,0.16,0.11,1)]),
	"Combat": [Color(0.91,0.83,0.66,1), Color(0.63,0.14,0.20,1), Color(0.89,0.23,0.26,1), Color(0.76,0.52,0.41,1), Color(0.24,0.15,0.19,1)],

"Athletic": [Color(0.76,0.76,0.81,1), Color(0.27,0.23,0.47,1), Color(0.29,0.35,0.67,1), Color(0.30,0.65,1,1), Color(0.19,0.24,0.30,1)],

"Recon": [Color(0.64,0.89,0.77,1), Color(0.32,0.64,0.57,1), Color(0.16,0.38,0.40,1), Color(0.78,1,0.95,1), Color(0.11,0.18,0.22,1)]
}


func init(initial_suit):
	set_suit(initial_suit, true)
	
func set_suit( suit, first: bool = false ):
	if !first:
		reset_modifiers()
	
	match suit:
		"Base":
			material.set_shader_param("helmet_color", suits[suit][0])
			material.set_shader_param("cloak_color", suits[suit][1])
			material.set_shader_param("shirt_color", suits[suit][2])
			material.set_shader_param("gloves_boots_color", suits[suit][3])
			material.set_shader_param("pants_color", suits[suit][4])
		"Combat":
			player.stats.set_atk_modifier(3)
			player.stats.set_max_hp_modifier(-ceil( (player.stats.max_health * 0.3) ))
			
			material.set_shader_param("helmet_color", suits[suit][0])
			material.set_shader_param("cloak_color", suits[suit][1])
			material.set_shader_param("shirt_color", suits[suit][2])
			material.set_shader_param("gloves_boots_color", suits[suit][3])
			material.set_shader_param("pants_color", suits[suit][4])
		"Athletic":
			player.MAXSPEED_modifier = 50
			
			material.set_shader_param("helmet_color", suits[suit][0])
			material.set_shader_param("cloak_color", suits[suit][1])
			material.set_shader_param("shirt_color", suits[suit][2])
			material.set_shader_param("gloves_boots_color", suits[suit][3])
			material.set_shader_param("pants_color", suits[suit][4])
		"Recon":
			player.set_radar(true)
			
			material.set_shader_param("helmet_color", suits[suit][0])
			material.set_shader_param("cloak_color", suits[suit][1])
			material.set_shader_param("shirt_color", suits[suit][2])
			material.set_shader_param("gloves_boots_color", suits[suit][3])
			material.set_shader_param("pants_color", suits[suit][4])
	
	
func reset_modifiers():
	player.stats.set_atk_modifier()
	player.stats.set_max_hp_modifier()
	player.MAXSPEED_modifier = 0
	player.set_radar(false)