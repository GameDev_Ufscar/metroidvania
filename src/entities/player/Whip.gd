extends Node2D

export var attack: int = 2

onready var player = get_parent().get_parent().get_parent()

func _ready():
	for i in get_children():
		i.add_to_group("whip_hitbox")


func strike(body):
	if body.is_in_group("enemies") :
		body.enemy_take_damage_and_knockback(attack, null, player.orientation, Vector2(128, 0), 0.2 )


func _on_hb_whip_h_body_entered(body):
	strike(body)


func _on_hb_whip_uh_body_entered(body):
	strike(body)


func _on_hb_whip_dh_body_entered(body):
	strike(body)


func _on_hb_whip_u_body_entered(body):
	strike(body)
	

func _on_hb_whip_d_body_entered(body):
	strike(body)