extends Node

signal health_changed(old_value, new_value)
signal health_depleted()

export var max_health: int = 3 setget set_max_hp

var health: int = max_health
export var attack: int = 1

func take_damage(hit):
	emit_signal("health_changed", health, health - hit)
	health -= hit
	health = max(0, health)
	if health == 0:
		
		emit_signal("health_depleted")

func heal(h):
	health += h
	health = min(max_health, health)

func set_max_hp(health):
	max_health = health
	heal(max_health)
	pass
