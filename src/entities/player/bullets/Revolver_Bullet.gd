extends "res://src/entities/scripts/Bullet.gd"

export (int) var MAXSPEED = 1000
export (int) var BOUNCE_COUNT = 3

onready var trail = $TrailNode/Trail

var speed = Vector2(1,1)

func _ready():
	add_to_group("player_bullets")
	speed *= direction
	speed.y *= MAXSPEED
	if(speed.x == 0 and speed.y == 0): 
		speed.x = orientation * MAXSPEED
	else:
		speed.x *= MAXSPEED

func _physics_process(delta):
	
	var collision = move_and_collide(speed * delta)
	
	if collision:
		handle_collision(collision)
		

func handle_collision(collision):
	if BOUNCE_COUNT > 0:
		BOUNCE_COUNT -= 1
		speed = speed.bounce(collision.normal)
	else:
		stop()
		
func _on_Hitbox_body_entered(body):
	if body.is_in_group("enemies"):
		if !$Hitbox/CollisionShape2D.is_disabled():
			body.enemy_take_damage_and_knockback(1, self)
			call_deferred("stop")

func stop():
	speed = Vector2(0, 0)
	$CollisionShape2D.set_disabled(true)
	$Hitbox/CollisionShape2D.set_disabled(true)
	$Sprite.visible = false
	trail.stop = true
