extends Node2D

onready var Global = $"/root/Global"

onready var att_anim = get_parent().get_parent().get_node( "anim" )
onready var machine_gun_timer = get_parent().get_parent().get_node( "ShootContinuousTimer" )
onready var sprite = get_parent()
onready var player = get_parent().get_parent()


# Scenes Variables
var Revolver_Bullet = preload("res://Revolver_Bullet.tscn")


func _ready():
	pass

func attack(attway, selected, anim):
	match selected: 
		"axe":
			if player.state == player.PLAYER.IDLE or player.state == player.PLAYER.AIR or player.state == player.PLAYER.MOVE:
				if(attway.y == 1):
					att_anim.play(anim + "_d")
				elif(attway.y == -1):
					att_anim.play(anim + "_u")
				else:
					att_anim.play(anim + "_h")
			else:
				att_anim.play(anim + "_h")
				
		"whip":
			if player.last_state == player.PLAYER.AIR:
				if(attway.y == 1):
					if(attway.x == 0):
						anim += "_d"
					else:
						anim += "_dh"
				elif(attway.y == -1):
					if(attway.x == 0):
						anim += "_u"
					else:
						anim += "_uh"
				else:
					anim += "_h"
			elif(player.last_state == player.PLAYER.IDLE or player.last_state == player.PLAYER.MOVE):
				if(attway.y == -1):
					if(attway.x == 0):
						anim += "_u"
					else:
						anim += "_uh"
				else:
					anim += "_h"
			elif(player.last_state == player.PLAYER.WALLSLIDE):
				if(attway.y == -1):
					anim += "_uh"
				elif(attway.y == 1):
					anim += "_uh"
				else:
					anim += "_h"
			else:
				anim += "_h"
			att_anim.play(anim, -1, 1.25)
		"gun":
			var projectile = Revolver_Bullet.instance()
			var pos
			
			if player.state == player.PLAYER.AIR:
				if(attway.y == 1):
					if(attway.x == 0):
						pos = $Gun/gun_d.global_position
						anim += "_d"
					else:
						anim += "_dh"
						pos = $Gun/gun_dh.global_position
				elif(attway.y == -1):
					if(attway.x == 0):
						anim += "_u"
						pos = $Gun/gun_u.global_position
					else:
						anim += "_uh"
						pos = $Gun/gun_uh.global_position
				else:
					anim += "_h"
					pos = $Gun/gun_h.global_position
					
			elif(player.state == player.PLAYER.IDLE or player.state == player.PLAYER.MOVE):
				if(attway.y == -1):
					if(attway.x == 0):
						anim += "_u"
						pos = $Gun/gun_u.global_position
					else:
						anim += "_uh"
						pos = $Gun/gun_uh.global_position
				else:
					anim += "_h"
					pos = $Gun/gun_h.global_position
					attway.y = 0
					
			elif(player.state == player.PLAYER.WALLSLIDE):
				#if(attway.y == -1):
				#	att_anim.play(anim + "_uh")
				#	pos = $Gun/gun_uh.global_position
				#elif(attway.y == 1):
				#	pos = $Gun/gun_dh.global_position
				#else:
				anim += "_h"
				pos = $Gun/gun_h.global_position
				attway.x = player.orientation
				
			else:
				anim += "_h"
				pos = $Gun/gun_h.global_position
			
			player.current_anim = anim
			att_anim.play(anim)
			
			if player.shoot:
				machine_gun_timer.start()
				
				projectile.global_position = pos
				projectile.init(attway, player.orientation)
				
				Global.game.add_child(projectile)
			elif player.is_shooting:
				if machine_gun_timer.is_stopped():
					machine_gun_timer.start()
					
					pos.x += randi() % 3
					pos.y += randi() % 3
					
					projectile.position = pos
					projectile.init(attway, player.orientation)
					
					Global.game.add_child(projectile)
				
