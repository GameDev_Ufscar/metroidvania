extends KinematicBody2D

# Global
onready var Global = $"/root/Global"

# Signals
signal orientation_changed(orientation)

# Preloads
# warning-ignore:unused_class_variable
onready var camera = $Camera2D
onready var sprite = $Sprite
onready var weapon = $Sprite/Weapon
onready var iframes = $iFramesTimer
onready var shoot_anim_timer = $ShootAnimTimer
onready var stats = $Stats
onready var anim = $anim
onready var gui = get_parent().get_node("GUI")


# Particles Effects
onready var dash_track_left = $Sprite/Particles/DashTrackLeft
onready var dash_track_right = $Sprite/Particles/DashTrackRight
onready var jump_dust_particles = $Sprite/Particles/JumpDust
onready var land_dust_particles = $Sprite/Particles/LandDust
onready var wallslide_dust_particles = $Sprite/Particles/WallslideDust
onready var walljumpleft_dust_particles = $Sprite/Particles/WalljumpDustLeft
onready var walljumpright_dust_particles = $Sprite/Particles/WalljumpDustRight


export (int) var MAXSPEED = 425
export (int) var SPRINT_MAXSPEED = 675
export (int) var JUMP_MAX_HEIGHT = -900
export (int) var MULTI_JUMP_MAX_HEIGHT = -800
export (int) var JUMP_MIN_HEIGHT = -400
export (int) var DASH_SPEED = 1200
export (int) var GROUND_ACC = 175
export (int) var AIR_ACC = 125
export (int) var GRAV = 30
export (int) var AIR_FINAL_SPEED = 1100
export (String) var WEAPON_MELEE = "whip"
export (String) var WEAPON_RANGED = "revolver"


# Player Attributtes
var HP_MAX = 5
var hp = HP_MAX


# Skill Activation
export (int) var MAX_MULTIJUMP = 0
export (bool) var WALLJUMP = false
export (bool) var DASH = false


# Movement Variables
var motion = Vector2(0,0)
var FALL_DIR = Vector2(0,-1)
var SPEED = 0
var dir = 0
var last_dir = 0
var orientation = 1
var WIND = Vector2(0,0)
var is_ground_jump = false


# Button Booleans
var jump = false
var sprint = false
var duck = false
var lookup = false
var attack = false
var shoot = false
var is_shooting = false
var aim = false
var input_made = false


# Player Control
var gravity_on = true
var has_control = true
var dash_input = false
var can_attack = true
var invicible = false
var current_attack


# Transitioning
var state
var last_state
var current_anim = "move"
var new_anim = ""


# Cooldowns - Frame Count and Aux
var air_momentum_cooldown_max = 25 # Frames the player keep its speed after walljumping
var walljump_cooldown_max = 15 # Frames the player keep sliding on wall
var ledgedrop_cooldown_max = 7 # Coyote Jump
export (int) var DASH_COOLDOWN_MAX = 10 # How further the dash goes
export (int) var DASH_COUNTER_COOLDOWN_MAX = 50 # How many frames to activate dash again
var air_momentum_cooldown = air_momentum_cooldown_max
var walljump_cooldown = walljump_cooldown_max
var ledgedrop_cooldown = ledgedrop_cooldown_max
var dash_cooldown = DASH_COOLDOWN_MAX
var dash_counter_cooldown = DASH_COUNTER_COOLDOWN_MAX
var hurt_state_cooldown = 0 # How many frames player stays in HURT state


# Player States
enum PLAYER {
	IDLE,
	MOVE,
	AIR,
	DASH,
	WALLSLIDE,
	ATTACK,
	HURT
}


# Skill Based
var multi_jump = MAX_MULTIJUMP


func _ready():
	add_to_group("player")
	state = PLAYER.IDLE
	last_state = PLAYER.IDLE
	has_control = true
	#stats.connect("health_depleted", self, "change_state")
	pass

var gameover = false

func _physics_process(delta):
	dir = 0
	restart_booleans()
	
	if stats.health == 0 and not gameover:
		has_control = 0
		gui.fade_in()
		gameover = true
	
#	if Input.is_action_pressed("restart"):
#		SpeedrunTimer.start_timer = false
#		get_tree().reload_current_scene()
	
	# Input Handler
	if has_control:
		handle_input()
	
	# Skills cooldowns
	calculate_cooldown()
	
	begin_player_state_machine()
	
	if(is_on_ceiling()):
		motion.y = 0
	
	if state != PLAYER.ATTACK:
		if( state == PLAYER.WALLSLIDE and orientation != (-1*last_dir) ):
			# When on Wallslide, turn away from the wall
			change_orientation(-1*last_dir)
		elif( orientation != sign(SPEED) and state != PLAYER.WALLSLIDE ):
			change_orientation(dir)
	
	motion.x = SPEED + WIND.x
	
	if (gravity_on and !is_on_floor()):
		motion.y = approach(motion.y, AIR_FINAL_SPEED, GRAV)
		motion.y = motion.y + WIND.y
	
	var snap = Vector2(0, 0)
	
	if state == PLAYER.IDLE or state == PLAYER.MOVE or state == PLAYER.ATTACK:
		snap = Vector2(0, 16)
	
	motion = move_and_slide_with_snap(motion, snap, FALL_DIR, true, 4, 0.79)
	
	if state != PLAYER.ATTACK:
		if shoot or is_shooting:
			shoot_anim_timer.start()
			var way = get_look_input()
			new_anim += "_shoot"
			weapon.attack(way, "gun", new_anim)
		elif shoot_anim_timer.is_stopped() and !aim:
			#if (current_anim != new_anim):
			#	current_anim = new_anim
			#	anim.play(current_anim)
			anim.play(new_anim)
		else:
			var way = get_look_input()
			new_anim += "_shoot"
			weapon.attack(way, "gun", new_anim)
	
	if !iframes.is_stopped():
		sprite.visible = !sprite.visible
	
	#$debug/hp.text = SpeedrunTimer.str_elapsed
	#$debug/motion.text = str(motion)
	#$debug/dir.text = str(get_look_input())
	#$debug/hp.text = str(current_anim)


func begin_player_state_machine():
	match state:
		PLAYER.IDLE:
			motion.y = 0
			
			new_anim = "idle"
			
			if attack and can_attack:
				change_state(PLAYER.ATTACK)
			elif jump:
				jump(JUMP_MAX_HEIGHT, true)
				change_state(PLAYER.AIR)
			elif dir != 0 and not aim:
				change_state(PLAYER.MOVE)
			elif !is_on_floor():
				change_state(PLAYER.AIR)
			elif dash_input:
				dash_cooldown = 0
				change_state(PLAYER.DASH)
		PLAYER.MOVE:
			SPEED = approach(SPEED, MAXSPEED*dir, GROUND_ACC)

			new_anim = "move"

			if attack and can_attack:
				change_state(PLAYER.ATTACK)
			elif(jump):
				jump(JUMP_MAX_HEIGHT, true)
				change_state(PLAYER.AIR)
			elif(SPEED == 0 and dir == 0):
				change_state(PLAYER.IDLE)
			elif aim:
				SPEED = 0
				change_state(PLAYER.IDLE)
			elif !is_on_floor():
				ledgedrop_cooldown = 0
				change_state(PLAYER.AIR)
			elif dash_input:
				dash_cooldown = 0
				change_state(PLAYER.DASH)
		PLAYER.AIR:
			if air_momentum_cooldown != air_momentum_cooldown_max:
				if air_momentum_cooldown > 6 and dir == (last_dir*(-1)):
					SPEED = approach(SPEED, MAXSPEED*dir, AIR_ACC)
					air_momentum_cooldown = air_momentum_cooldown_max
				else:
					SPEED = approach(SPEED, MAXSPEED*last_dir, AIR_ACC)
			else:
				SPEED = approach(SPEED, MAXSPEED*dir, AIR_ACC)
			
			new_anim = "air"
			
			if motion.y < 0:
				new_anim += "_rise"
			else:
				new_anim += "_fall"
			
			if jump:
				if ledgedrop_cooldown < ledgedrop_cooldown_max:
					jump(JUMP_MAX_HEIGHT)
					ledgedrop_cooldown = ledgedrop_cooldown_max 
				elif(multi_jump > 0):
					motion.y = MULTI_JUMP_MAX_HEIGHT
					multi_jump -= 1
			
			if attack and can_attack:
				change_state(PLAYER.ATTACK)
			elif is_on_wall() && WALLJUMP && motion.y > -90:
				motion.y = 0
				last_dir = sign(SPEED)
				walljump_cooldown = 0
				change_state(PLAYER.WALLSLIDE)
			elif(is_on_floor()):
				multi_jump = MAX_MULTIJUMP
				if(SPEED == 0):
					change_state(PLAYER.IDLE)
				else:
					change_state(PLAYER.MOVE)
				land_dust_particles.emitting = true
			elif dash_input:
				dash_cooldown = 0
				change_state(PLAYER.DASH)
		PLAYER.DASH:
			
			if orientation == 1:
				dash_track_right.emitting = true
			else:
				dash_track_left.emitting = true
			has_control = false
			gravity_on = false
			
			new_anim = "dash"
			
			shoot_anim_timer.stop()
			
			SPEED = DASH_SPEED*orientation
			motion.y = 0
			
			if is_on_wall() && WALLJUMP:
				motion.y = 0
				last_dir = sign(SPEED)
				dash_track_left.emitting = false
				dash_track_right.emitting = false
				has_control = true
				change_state(PLAYER.WALLSLIDE)
			
			if(dash_cooldown == DASH_COOLDOWN_MAX):
				dash_track_left.emitting = false
				dash_track_right.emitting = false
				has_control = true
				if(is_on_floor()):
					if(SPEED == 0):
						change_state(PLAYER.IDLE)
					else:
						change_state(PLAYER.MOVE)
				else:
					change_state(PLAYER.AIR)
		PLAYER.WALLSLIDE:
			gravity_on = false
			
			new_anim = "wallslide"
			
			motion.y = approach(motion.y, 100, GRAV)
			
			if motion.y > 96:
				wallslide_dust_particles.emitting = true
				
			# If player let go of the d-pad
			if last_dir != dir or dir == 0:
				walljump_cooldown = min(walljump_cooldown + 1, walljump_cooldown_max)
				if walljump_cooldown == walljump_cooldown_max:
					SPEED = 0
					change_state(PLAYER.AIR)
					wallslide_dust_particles.emitting = false
			
			if(jump):
				jump(JUMP_MAX_HEIGHT)
				last_dir *= -1
				SPEED = last_dir*MAXSPEED
				gravity_on = true
				air_momentum_cooldown = 0
				
				change_state(PLAYER.AIR)
				
				if last_dir == 1:
					walljumpleft_dust_particles.emitting = true
				else:
					walljumpright_dust_particles.emitting = true
					
				wallslide_dust_particles.emitting = false
			if(is_on_floor()):
				if(SPEED == 0):
					change_state(PLAYER.IDLE)
				else:
					change_state(PLAYER.MOVE)
					
				wallslide_dust_particles.emitting = false
			if(!is_on_wall()):
				SPEED = 0
				change_state(PLAYER.AIR)
				
				wallslide_dust_particles.emitting = false
				
		PLAYER.ATTACK:
			new_anim = "attack"
			
			# ATTACK ANIMATION TRIGGERS ONLY ONCE #
			if can_attack == true:
				var way = get_look_input()
				weapon.attack(way, WEAPON_MELEE, "attack_" + WEAPON_MELEE)
				can_attack = false
			
			if is_on_floor():
				SPEED = approach(SPEED, 0, GROUND_ACC)
			else:
				if air_momentum_cooldown != air_momentum_cooldown_max:
					if air_momentum_cooldown > 6 and dir == (last_dir*(-1)):
						SPEED = approach(SPEED, MAXSPEED*dir, AIR_ACC)
						air_momentum_cooldown = air_momentum_cooldown_max
					else:
						SPEED = approach(SPEED, MAXSPEED*last_dir, AIR_ACC)
				else:
					SPEED = approach(SPEED, MAXSPEED*dir, AIR_ACC)
					
		PLAYER.HURT:
			wallslide_dust_particles.emitting = false
			dash_track_left.emitting = false
			dash_track_right.emitting = false
			
			new_anim = "hurt"
			
			shoot_anim_timer.stop()
			
			if is_on_floor():
				SPEED = approach(SPEED, MAXSPEED*dir, 16)
			
			if hurt_state_cooldown == 0:
				has_control = true
				
				can_attack = true
				
				iframes.start(1)
				
				SPEED = 0
				
				if is_on_floor():
					change_state(PLAYER.IDLE)
				else:
					change_state(PLAYER.AIR)
			pass

func jump(jump_height, is_ground_jump: bool = false):
	motion.y = jump_height
	if is_ground_jump:
		jump_dust_particles.emitting = true
	self.is_ground_jump = is_ground_jump
	# short_hop_cooldown = 15

# returns current frame vector of directioal input
func get_look_input():
	var attackway = Vector2()
	attackway.x = dir
	attackway.y = 0
	
	if duck:
		attackway.y = 1
	if lookup:
		attackway.y = -1
		
	return attackway

func calculate_cooldown():
	dash_cooldown = min(dash_cooldown + 1, DASH_COOLDOWN_MAX)
	dash_counter_cooldown = min(dash_counter_cooldown + 1, DASH_COUNTER_COOLDOWN_MAX)
	#walljump_cooldown = max(walljump_cooldown - 1, 0)
	air_momentum_cooldown = min(air_momentum_cooldown + 1, air_momentum_cooldown_max)
	ledgedrop_cooldown = min(ledgedrop_cooldown + 1, ledgedrop_cooldown_max)
	hurt_state_cooldown = max(hurt_state_cooldown - 1, 0)
	# short_hop_cooldown = max(short_hop_cooldown - 1, 0)

func restart_booleans():
	jump = false
	sprint = false
	duck = false
	lookup = false
	attack = false
	shoot = false
	is_shooting = false
	aim = false
	dash_input = false
	gravity_on = true
	is_ground_jump = false

func handle_input():
	
	if Global.paused:
		return
	
	var key_left = 0
	var key_right = 0
	
	if Input.is_action_pressed("ui_left"):
		key_left = -1
	if Input.is_action_pressed("ui_right"):
		key_right = 1
		
	dir = key_left + key_right
		
	if Input.is_action_just_pressed("ui_accept"):
		jump = true
	if(Input.is_action_just_released("ui_accept")):
		if(motion.y  < JUMP_MIN_HEIGHT):
			motion.y = JUMP_MIN_HEIGHT
	if Input.is_action_pressed("ui_down"):
		duck = true
	if Input.is_action_pressed("ui_up"):
		lookup = true
		
	if Input.is_action_just_pressed("dash") && DASH:
		if dash_counter_cooldown == DASH_COUNTER_COOLDOWN_MAX:
			dash_input = true
			dash_counter_cooldown =  0
	if Input.is_action_pressed("sprint"):
		sprint = true
	if Input.is_action_just_pressed("attack"):
		attack = true
		current_attack = WEAPON_MELEE
	if Input.is_action_just_pressed("shoot"):
		shoot = true
		current_attack = WEAPON_RANGED
	if Input.is_action_pressed("shoot"):
		is_shooting = true
	if Input.is_action_pressed("aim"):
		aim = true
		
	if Input.is_action_pressed("debug_take_hit"):
		player_take_damage_and_knockback(1)


func change_state(new_state):
	if(new_state != state):
		last_state = state
		state = new_state


func change_orientation(ori):
	if(orientation != ori and ori != 0):
		orientation = ori
		sprite.scale.x *= -1
		emit_signal("orientation_changed", orientation)


func approach(a, b, amount):
	if (a < b):
		a += amount
		if (a > b):
			return b
	else:
		a -= amount
		if(a < b):
			return b
	return a


func _on_anim_animation_finished(anim_name):
	if anim_name.begins_with("attack"):
		if(is_on_floor()):
			if(SPEED == 0):
				change_state(PLAYER.IDLE)
			else:
				change_state(PLAYER.MOVE)
		else:
			change_state(PLAYER.AIR)
			
		can_attack = true


func player_take_damage_and_knockback(
		damage: int = 0,
		ori: int = 0,
		kb_speed: Vector2 = Vector2(128, -128),
		hurt_state_cooldown: int = 14):
	change_state(PLAYER.HURT)
	
	stats.take_damage(damage)
	
	if ori != 0:
		SPEED = kb_speed.x * ori
	else:
		SPEED = kb_speed.x * orientation * -1
	
	jump(kb_speed.y)
	
	has_control = false
	
	self.set_collision_mask_bit(3, false)
	
	if stats.health == 0:
		self.hurt_state_cooldown = INF
	else:
		self.hurt_state_cooldown = hurt_state_cooldown


func _on_iFramesTimer_timeout():
	sprite.visible = true
	self.set_collision_mask_bit(3, true)
	pass # Replace with function body.


func _on_Stats_health_changed(old_value, new_value):
#	gui.set_hp_value(new_value)
	pass # Replace with function body.


func _on_Stats_health_depleted():
	pass # Replace with function body.


func change_cloak_color(new_color):
	sprite.change_color(new_color)