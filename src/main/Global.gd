extends Node

# warning-ignore:unused_class_variable
onready var game = $"/root/MainScene/ViewportContainer/GameViewport/Game"
# warning-ignore:unused_class_variable
onready var game_viewport = $"/root/MainScene/ViewportContainer/GameViewport"
# warning-ignore:unused_class_variable
onready var game_viewport_container = $"/root/MainScene/ViewportContainer"
# warning-ignore:unused_class_variable
onready var player = game.get_node("Player")
# warning-ignore:unused_class_variable
onready var minimap = $"/root/MainScene/GUI/Minimap"
# warning-ignore:unused_class_variable
onready var minimap_viewport = $"/root/MainScene/GUI/Minimap/MinimapViewport"
# warning-ignore:unused_class_variable
onready var gui = $"/root/MainScene/GUI/Minimap/MinimapViewport/GUITest"
# warning-ignore:unused_class_variable
onready var paused = false