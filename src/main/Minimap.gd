extends ViewportContainer

export(float) var MAX_ZOOM = 10
export(float) var MIN_ZOOM = 1
export(float) var DEFULT_ZOOM = 1.5
export(float) var ZOOM_SPEED = 3
export(float) var ZOOM_ACCEL = 0.1
export(float) var MOVE_SPEED = 20

var zoom_accel = 0.0
var activated = false
var radar_enabled = false

onready var Global = $"/root/Global"
onready var Camera = $MinimapViewport/MinimapCamera

func _ready():
	pass
	
# warning-ignore:unused_argument
func _process(delta):
	
	if Input.is_action_just_pressed("ui_map"):
		toggle_minimap()
		
	if !Global.paused:
		return
		
	var move = Vector2(0, 0)
	
	if Input.is_action_pressed("ui_left"):
		move.x -= 1
	if Input.is_action_pressed("ui_right"):
		move.x += 1
	if Input.is_action_pressed("ui_up"):
		move.y -= 1
	if Input.is_action_pressed("ui_down"):
		move.y += 1
		
	move *= MOVE_SPEED
		
	Camera.offset += move
	
	var zoom = 0
	
	if Input.is_action_pressed("minimap_zoom_in") or Input.is_action_pressed("minimap_zoom_out"):
		zoom_accel += ZOOM_ACCEL
	else:
		zoom_accel -= ZOOM_ACCEL * 2
		
	zoom_accel = min(zoom_accel, ZOOM_SPEED)
	zoom_accel = max(zoom_accel, 0)
	
	if Input.is_action_pressed("minimap_zoom_in"):
		zoom += ZOOM_SPEED + zoom_accel
	if Input.is_action_pressed("minimap_zoom_out"):
		zoom -= ZOOM_SPEED + zoom_accel
		
	Camera.zoom += Vector2(1, 1) * zoom * delta
	
	if Camera.zoom.x >= MAX_ZOOM:
		Camera.zoom = Vector2(MAX_ZOOM, MAX_ZOOM)
	if Camera.zoom.x <= MIN_ZOOM:
		Camera.zoom = Vector2(MIN_ZOOM, MIN_ZOOM)
	
func toggle_minimap():
	
	# Activates minimap
	activated = !activated
	
	# Pauses the game
	Global.paused = !Global.paused
	
	# Make only the minimap and minimap hud elements visible
	self.visible = !self.visible
	
	for MinimapInfo in get_tree().get_nodes_in_group("MinimapInfo"):
		if MinimapInfo.is_in_group("MinimapInfoRadar"):
			if radar_enabled:
				MinimapInfo.visible = !MinimapInfo.visible
		else:
			MinimapInfo.visible = !MinimapInfo.visible
	
	# Resets the minimap position
	Camera.offset = Vector2(0, 0)
	Camera.zoom = Vector2(1.5, 1.5)
	Camera.position = Vector2(Global.player.global_position.x, Global.player.global_position.y)
