extends Area2D


export (String) var TYPE = "body"


onready var door = get_parent().get_node("Door")


func _ready():
	var node_function = "_on_" + name + "_" + TYPE + "_entered"
	if TYPE == "body":
		connect("body_entered", door, node_function)
	elif TYPE == "area":
		connect("area_entered", door, node_function)
	elif TYPE == "both":
		connect("body_entered", door, "_on_" + name + "_body_entered")
		connect("area_entered", door, "_on_" + name + "_area_entered")
