extends Area2D

onready var camera_bounds = get_node("../CameraBounds")

func _ready():
	self.connect("body_entered", camera_bounds, "on_BossTrigger_body_entered")
	self.connect("body_entered", self, "on_BossTrigger_body_entered")

func _on_BossTrigger_body_entered(body):
	if body.name == "Player":
		queue_free()
