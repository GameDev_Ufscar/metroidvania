extends Node2D

onready var player = get_tree().get_nodes_in_group("player")[0]
onready var level = get_parent()
onready var camera = player.get_node("Camera2D")

func _ready():
	add_to_group("camera_bounds")
	
	var camera = player.get_node("Camera2D")
	
	camera.limit_top = $TopLeft.position.y + level.position.y
	camera.limit_left = $TopLeft.position.x + level.position.x
	camera.limit_bottom = $BottomRight.position.y + level.position.y
	camera.limit_right = $BottomRight.position.x + level.position.x
	


func _on_BossTrigger_body_entered(body):
	if body.name == "Player":
		camera.limit_top = $BossTopLeft.position.y
		camera.limit_left = $BossTopLeft.position.x
		camera.limit_bottom = $BossBottomRight.position.y
		camera.limit_right = $BossBottomRight.position.x
