extends Area2D

onready var gui = get_tree().get_nodes_in_group("gui")[0]

func _on_Area2D_body_entered(body):
	if body.is_in_group("player"):
		for i in get_tree().get_nodes_in_group("destructables"):
			i.destroy()
		
		if body.stats.health == 10:
			gui.fade_in_final_perfect()
		else:
			gui.fade_in_final()
	