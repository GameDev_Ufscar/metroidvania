extends Area2D

onready var player = get_tree().get_nodes_in_group("player")

func _ready():
	pass

func _process(delta):
	self.scale += Vector2(0.01, 0)

func _on_HazardZone_body_entered(body):
	
	if body.name == "Player":
		body.player_take_damage_and_knockback(1, 1, Vector2(512, -256), 30)
	
	pass
